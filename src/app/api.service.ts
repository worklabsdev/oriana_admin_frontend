import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import "rxjs/operators";

@Injectable()
export class ApiService {
  public serverurl: string = "http://139.59.89.86:8080/";
  public baseurl: string = "http://139.59.89.86:8080";
  // public baseurl: string = "http://46.101.246.231:8080";
  // public serverurl: string = "http://46.101.246.231:8080/";
  // public baseurl: string = "http://localhost:8080";
  // public serverurl: string = "http://localhost:8080/";
  //private baseurl: string = "http://localhost:8080/api/v1";
  //public serverurl: string = "http://159.65.69.180:8080/";
  //private baseurl: string = "http://159.65.69.180:8080/api/v1";
  //private baseurl:string = "http://wedding-kart-mickey96.c9users.io";

  public loggedIn: string;
  public cartData = [];
  public cartQty: any;
  public userData = [];
  public cModal: any = "";
  public messages = [];
  public adminLogged: any = "";

  constructor(private http: HttpClient, private cookies: CookieService) {}

  public get(url) {
    let httpOptions = {
      headers: new HttpHeaders({
        "x-auth-token": this.cookies.get("orianaAdminToken")
      })
    };
    console.log("this is auth token", this.cookies.get("orianaAdminToken"));
    return this.http.get(this.baseurl + url, httpOptions);
  }

  public post(url, data) {
    let httpOptions = {
      headers: new HttpHeaders({
        "x-auth-token": this.cookies.get("orianaAdminToken")
      })
    };
    console.log("this is auth token", this.cookies.get("orianaAdminToken"));
    return this.http.post(this.baseurl + url, data, httpOptions);
  }

  public put(url, data) {
    let httpOptions = {
      headers: new HttpHeaders({
        "x-auth-token": this.cookies.get("orianaAdminToken")
      })
    };
    console.log("this is auth token", this.cookies.get("orianaAdminToken"));
    return this.http.put(this.baseurl + url, data, httpOptions);
  }

  public delete(url) {
    let httpOptions = {
      headers: new HttpHeaders({
        "x-auth-token": this.cookies.get("orianaAdminToken")
      })
    };
    console.log("this is auth token", this.cookies.get("orianaAdminToken"));
    return this.http.delete(this.baseurl + url, httpOptions);
  }
}
