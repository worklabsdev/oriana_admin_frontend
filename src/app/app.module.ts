import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { AppRoutes } from "./app.routes";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { ToastrModule } from "ngx-toastr";
import { NgxSpinnerModule } from "ngx-spinner";
import { ApiService } from "./api.service";
import { CookieService } from "ngx-cookie-service";

import { AppComponent } from "./app.component";
import { BuyerSellerComponent } from "./buyer-seller/buyer-seller/buyer-seller.component";
import { EditBuyerSellerComponent } from "./buyer-seller/edit-buyer-seller/edit-buyer-seller.component";
import { EditCategoryComponent } from "./categories/edit-category/edit-category.component";
import { CategoriesComponent } from "./categories/categories/categories.component";
import { ProductsComponent } from "./products/products/products.component";
import { EditProductComponent } from "./products/edit-product/edit-product.component";
import { ShopsComponent } from "./shops/shops/shops.component";
import { EditShopComponent } from "./shops/edit-shop/edit-shop.component";
import { OrdersComponent } from "./orders/orders/orders.component";
import { ViewOrderComponent } from "./orders/view-order/view-order.component";
import { CancellationRequestComponent } from "./orders/cancellation-request/cancellation-request.component";
import { HomeComponent } from "./dashboard/home/home.component";
import { FundsComponent } from "./dashboard/funds/funds.component";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { LoginComponent } from "./login/login.component";
import { ProfileComponent } from "./dashboard/profile/profile.component";
import { SubCategoriesComponent } from "./sub-categories/sub-categories/sub-categories.component";
import { EditSubCategoryComponent } from "./sub-categories/edit-sub-category/edit-sub-category.component";
import { ViewBuyerSellerComponent } from "./buyer-seller/view-buyer-seller/view-buyer-seller.component";
import { SellerComponent } from './buyer-seller/seller/seller.component';
import { EditSellerComponent } from './buyer-seller/edit-seller/edit-seller.component';
import { ViewSellerComponent } from './buyer-seller/view-seller/view-seller.component';
import { AddCategoryComponent } from './categories/add-category/add-category.component';

@NgModule({
  declarations: [
    AppComponent,
    BuyerSellerComponent,
    EditBuyerSellerComponent,
    EditCategoryComponent,
    CategoriesComponent,
    ProductsComponent,
    EditProductComponent,
    ShopsComponent,
    EditShopComponent,
    OrdersComponent,
    ViewOrderComponent,
    CancellationRequestComponent,
    HomeComponent,
    FundsComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    ProfileComponent,
    SubCategoriesComponent,
    EditSubCategoryComponent,
    ViewBuyerSellerComponent,
    SellerComponent,
    EditSellerComponent,
    ViewSellerComponent,
    AddCategoryComponent
  ],

  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    // NgxPaginationModule,
    NgxSpinnerModule,
    HttpClientModule,
    BrowserAnimationsModule,
    HttpModule,
    AppRoutes,
    ToastrModule.forRoot()
  ],
  providers: [HttpModule, CookieService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule {}
