import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { ApiService } from "../../api.service";
import { CookieService } from "ngx-cookie-service";
import "rxjs";
import { map } from "rxjs/operators";
import { DomSanitizer } from "@angular/platform-browser";
declare var $: any;
@Component({
  selector: "app-buyer-seller",
  templateUrl: "./buyer-seller.component.html",
  styleUrls: ["./buyer-seller.component.css"]
})
export class BuyerSellerComponent implements OnInit {
  public allBuyers = [];
  public showBuyers: any = "";
  public currPage: number = 1;
  public pageNum: number = 1;
  public buyer = [];
  name: string = "";
  phone: string = "";
  email: string = "";
  aname: string = "";
  aphone: string = "";
  aemail: string = "";
  constructor(
    private apis: ApiService,
    private toast: ToastrService,
    /*private spinner: NgxSpinnerService, */
    private cookies: CookieService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getAllBuyers();
  }

  previous() {
    if (this.currPage == 1) {
      this.toast.error("You Are Already on the 1st Page");
    } else {
      this.currPage -= 1;
      this.getAllBuyers();
    }
  }

  next() {
    this.currPage += 1;
    this.getAllBuyers();
  }

  getAllBuyers() {
    this.apis
      .post("/api/v1/admin/users", {
        token: this.cookies.get("orianaAdminToken"),
        page: this.currPage,
        filter: "buyer"
      })

      .subscribe(
        (res: Response) => {
          console.log(res);
          if (res["message"] == "No User Found") {
            this.toast.error("No more users are there");
          }
          if (res["status"].toString() == "success") {
            this.allBuyers = res["data"]["list"];
            if (this.allBuyers.length != 0) {
              this.showBuyers = 1;
            } else {
              this.showBuyers = "";
            }
            //this.currPage = res["data"].currPage;
            this.pageNum = this.currPage;
            console.log("allBuyers", this.allBuyers);
            console.log("currPage", this.currPage);
          }
          // else {
          //   this.toast.error("Some Problem Occured");
          // }
        },
        (error: any) => {
          console.log("this is the error ", error);
        }
      );
  }

  deleteBuyer(id) {
    this.apis
      .post("/api/v1/admin/user/delete", {
        token: this.cookies.get("orianaAdminToken"),
        _id: id
      })
      .subscribe(
        (res: Response) => {
          console.log(res);
          if (res["status"].toString() == "success") {
            this.toast.success("Deleted");
            this.getAllBuyers();
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (error: any) => {
          console.log("this is the error ", error);
        }
      );
  }
  disableBuyer(id) {
    console.log(id);
    this.apis
      .post("/api/v1/admin/user/inactivate", {
        token: this.cookies.get("orianaAdminToken"),
        user_id: id
      })
      .subscribe(
        (res: Response) => {
          console.log(res);
          if (res["status"].toString() == "success") {
            this.toast.success("Blocked");
            this.getAllBuyers();
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (error: any) => {
          console.log("this is the error ", error);
        }
      );
  }

  enableBuyer(id) {
    this.apis
      .post("/api/v1/admin/user/activate", {
        token: this.cookies.get("orianaAdminToken"),
        user_id: id
      })
      .subscribe(
        (res: Response) => {
          console.log(res);
          if (res["status"].toString() == "success") {
            this.toast.success("Unblocked");
            this.getAllBuyers();
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (error: any) => {
          console.log("this is the error ", error);
        }
      );
  }

  getBuyerDetails(id) {
    this.apis
      .post("/api/v1/admin/user", {
        token: this.cookies.get("orianaAdminToken"),
        user_id: id
      })
      .subscribe(
        (res: Response) => {
          console.log("buyerInfo");
          console.log(res);
          if (res["status"].toString() == "success") {
            this.buyer = res["data"];
            this.name = this.buyer["name"];
            this.email = this.buyer["email"];
            this.phone = this.buyer["phone"];
            console.log(this.buyer);
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (err: Response) => {
          console.log(err);
          if (err["error"].message == "Errors") {
            this.toast.error(err["error"].data.errors[0].msg);
          } else {
            this.toast.error(err["error"].message);
          }
        }
      );
  }

  updateBuyerDetails() {
    this.apis
      .post("/api/v1/admin/user/update", {
        token: this.cookies.get("orianaAdminToken"),
        _id: this.buyer["_id"],
        name: this.name,
        email: this.email,
        phone: this.phone
      })
      .subscribe(
        (res: Response) => {
          console.log("buyerUpdate");
          console.log(res);
          if (res["status"].toString() == "success") {
            this.toast.success("Updated");

            this.getAllBuyers();
            this.name = "";
            this.email = "";
            this.phone = "";
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (err: Response) => {
          console.log(err);
          if (err["error"].message == "Errors") {
            this.toast.error(err["error"].data.errors[0].msg);
          } else {
            this.toast.error(err["error"].message);
          }
        }
      );
  }

  addNewBuyer() {
    this.apis
      .post("/api/v1/admin/user/add", {
        token: this.cookies.get("orianaAdminToken"),
        name: this.aname,
        email: this.aemail,
        phone: this.aphone
      })
      .subscribe(
        (res: Response) => {
          console.log("buyerInfo");
          console.log(res);
          if (res["status"].toString() == "success") {
            this.toast.success("Added");

            this.getAllBuyers();
            this.aname = "";
            this.aemail = "";
            this.aphone = "";
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (err: Response) => {
          console.log(err);
          if (err["error"].message == "Errors") {
            this.toast.error(err["error"].data.errors[0].msg);
          } else {
            this.toast.error(err["error"].message);
          }
        }
      );
  }
}
