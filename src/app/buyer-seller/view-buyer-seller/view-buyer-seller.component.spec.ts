import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBuyerSellerComponent } from './view-buyer-seller.component';

describe('ViewBuyerSellerComponent', () => {
  let component: ViewBuyerSellerComponent;
  let fixture: ComponentFixture<ViewBuyerSellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBuyerSellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBuyerSellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
