import { Component, OnInit } from "@angular/core";
import { BrowserModule, SafeStyle } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { ApiService } from "../../api.service";
//import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from "ngx-toastr";
declare var $: any;
declare var jQuery: any;
@Component({
  selector: "app-view-buyer-seller",
  templateUrl: "./view-buyer-seller.component.html",
  styleUrls: ["./view-buyer-seller.component.css"]
})
export class ViewBuyerSellerComponent implements OnInit {
  public buyer = [];
  public allOrders = [];
  bName: string = "";
  bPhone: string = "";
  bEmail: string = "";
  showOrders: any = "";
  constructor(
    private route: ActivatedRoute,
    private cookies: CookieService,
    private router: Router,
    /* private spinner:NgxSpinnerService, */
    private apis: ApiService,
    private toast: ToastrService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      var id = params["id"];
      console.log("id");
      console.log(id);
      // Api to get one product single product
      this.apis
        .post("/api/v1/admin/user", {
          token: this.cookies.get("orianaAdminToken"),
          user_id: id
        })
        .subscribe(
          (res: Response) => {
            console.log("buyerInfo");
            console.log(res);
            if (res["status"].toString() == "success") {
              this.buyer = res["data"];
              this.bName = this.buyer["name"];
              this.bEmail = this.buyer["email"];
              this.bPhone = this.buyer["phone"];
              console.log(this.buyer);
            } else {
              this.toast.error("Some Problem Occured");
            }
          },
          (err: Response) => {
            console.log(err);
            if (err["error"].message == "Errors") {
              this.toast.error(err["error"].data.errors[0].msg);
            } else {
              this.toast.error(err["error"].message);
            }
          }
        );

      this.getAllOrders(id);
    });
  }

  public getAllOrders(id) {
    this.apis
      .post("/api/v1/buyer/orders/", {
        buyer_id: id
      })
      .subscribe(
        (res: Response) => {
          console.log("AllOrders");
          console.log(res);
          if (res["status"].toString() == "success") {
            res["data"].map(obj => {
              obj["mainImage"]
                ? (obj["mainImage"] = this.apis.serverurl + obj["mainImage"])
                : (obj["mainImage"] = "assets/img/users/THUMB");
            });
            this.allOrders = res["data"];
            if (this.allOrders && this.allOrders.length > 0) {
              this.showOrders = 1;
            } else {
              this.showOrders = "";
            }
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (res: Response) => {
          console.log(res);
          if (res["error"].message == "Errors") {
            this.toast.error(res["error"].data.errors[0].msg);
          } else {
            this.toast.error(res["error"].message);
          }
        }
      );
  }
}
