import { Component, OnInit } from "@angular/core";
import { BrowserModule, SafeStyle } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { ApiService } from "../../api.service";
//import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from "ngx-toastr";
declare var $: any;
declare var jQuery: any;
@Component({
  selector: "app-view-seller",
  templateUrl: "./view-seller.component.html",
  styleUrls: ["./view-seller.component.css"]
})
export class ViewSellerComponent implements OnInit {
  public allShops = [];
  public shop: any = {};
  profilePicBtn: Boolean = false;
  profilePic: any = "";
  list: any = "";
  documents: any = "";
  public seller = [];
  sName: string = "";
  sPhone: string = "";
  sEmail: string = "";
  constructor(
    private route: ActivatedRoute,
    private cookies: CookieService,
    private router: Router,
    /* private spinner:NgxSpinnerService, */
    private apis: ApiService,
    private toast: ToastrService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      var id = params["id"];
      console.log("id");
      console.log(id);
      // Api to get one product single product
      this.apis
        .post("/api/v1/admin/seller", {
          token: this.cookies.get("orianaAdminToken"),
          user_id: id
        })
        .subscribe(
          (res: Response) => {
            console.log("sellerInfo");
            console.log(res);
            if (res["status"].toString() == "success") {
              this.seller = res["data"];
              this.sName = this.seller["name"];
              this.sEmail = this.seller["email"];
              this.sPhone = this.seller["phone"];
              console.log(this.seller);
            } else {
              this.toast.error("Some Problem Occured");
            }
          },
          (err: Response) => {
            console.log(err);
            if (err["error"].message == "Errors") {
              this.toast.error(err["error"].data.errors[0].msg);
            } else {
              this.toast.error(err["error"].message);
            }
          }
        );

      this.getAllShops(id);
    });
  }

  getAllShops(id) {
    this.apis
      .post("/api/v1/seller/shops", {
        seller_id: id
      })
      .subscribe(
        (res: Response) => {
          console.log(res);

          if (res["status"].toString() == "success") {
            this.allShops = res["data"];
            if (this.allShops.length != 0) {
              this.list = 1;
              if (this.allShops[0]["shopManagementPic"].length != 0) {
                this.documents = 1;
              } else {
                this.documents = "";
              }
            } else {
              this.list = "";
            }

            console.log("allShops", this.allShops);
          }
          // else {
          //   this.toast.error("Some Problem Occured");
          // }
        },
        (error: any) => {
          console.log("this is the error ", error);
        }
      );
  }
}
