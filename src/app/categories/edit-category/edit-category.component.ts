import { Component, OnInit } from "@angular/core";
import { BrowserModule, SafeStyle } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { ApiService } from "../../api.service";
//import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from "ngx-toastr";
declare var $: any;
declare var jQuery: any;

@Component({
  selector: "app-edit-category",
  templateUrl: "./edit-category.component.html",
  styleUrls: ["./edit-category.component.css"]
})
export class EditCategoryComponent implements OnInit {
  categoryId: any = "";
  subCatArray = [];
  newSubCat: string = "";
  name: string = "";
  constructor(
    private route: ActivatedRoute,
    private cookies: CookieService,
    private router: Router,
    /* private spinner:NgxSpinnerService, */
    private apis: ApiService,
    private toast: ToastrService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      var id = params["id"];
      this.categoryId = id;
      console.log("id");
      console.log(id);
      this.getCategory(id);
    });
  }

  deleteSubCat(i) {
    this.subCatArray.splice(i, 1);
  }

  addSubCat() {
    if (this.newSubCat != "") {
      this.subCatArray.push(this.newSubCat);
      this.newSubCat = "";
      console.log("this.subCatArray");
      console.log(this.subCatArray);
    }
  }

  getCategory(cat_id) {
    this.apis.get("/api/v1/admin/getCategory/" + cat_id).subscribe(
      (res: Response) => {
        console.log("getCat");
        console.log(res);
        if (res["status"].toString() == "success") {
          this.name = res["data"][0].name;
          this.subCatArray = res["data"][0].subcategories[0].split(",");
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
        // if (res["error"].message == "Errors") {
        //   this.toast.error(res["error"].data.errors[0].msg);
        // } else {
        //   this.toast.error(res["error"].message);
        // }
        // this.toast.error("Authentication Failed");
      }
    );
  }

  updateCategory() {
    if (this.subCatArray.length != 0) {
      this.apis
        .put("/api/v1/admin/updateCategory", {
          token: this.cookies.get("orianaAdminToken"),
          name: this.name,
          sub_cat: this.subCatArray,
          cat_id: this.categoryId
        })
        .subscribe(
          (res: Response) => {
            console.log("addCat");
            console.log(res);
            if (res["status"].toString() == "success") {
              this.toast.success("Added Successfully");
              this.router.navigate(["/categories"]);
            } else {
              this.toast.error("Some Problem Occured");
            }
          },
          (res: Response) => {
            console.log(res);
            // if (res["error"].message == "Errors") {
            //   this.toast.error(res["error"].data.errors[0].msg);
            // } else {
            //   this.toast.error(res["error"].message);
            // }
            this.toast.error("Authentication Failed");
          }
        );
    } else {
      this.toast.error("Please Add Subcategory");
    }
  }
}
