import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { ApiService } from "../../api.service";
import { CookieService } from "ngx-cookie-service";
import "rxjs";
@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.css"]
})
export class ProfileComponent implements OnInit {
  oldpassword: any = "";
  newpassword: any = "";
  cpassword: any = "";
  constructor(
    private apis: ApiService,
    private toast: ToastrService,
    /*private spinner: NgxSpinnerService, */
    private cookies: CookieService,
    private router: Router
  ) {}

  ngOnInit() {}

  changePassword() {
    this.apis
      .post("/api/v1/admin/changepassword", {
        token: this.cookies.get("orianaAdminToken"),
        oldpassword: this.oldpassword,
        newpassword: this.newpassword,
        cpassword: this.cpassword
      })
      .subscribe(
        (res: Response) => {
          console.log("buyerInfo");
          console.log(res);
          if (res["status"].toString() == "success") {
            this.router.navigate(["/dashboard"]);
            this.toast.success("Password Changed Successfully");
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (err: Response) => {
          console.log(err);
          if (err["error"].message == "Errors") {
            this.toast.error(err["error"].data.errors[0].msg);
          } else {
            this.toast.error(err["error"].message);
          }
        }
      );
  }
}
