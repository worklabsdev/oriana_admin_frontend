import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  public display: string = "none";
  public femail: any;
  public lemail: any;
  public lpassword: any;
  public rname: any;
  public remail: any;
  public rpassword: any;
  public cModal: any = "";
  public regMsg: any;
  constructor(
    public apis: ApiService,
    private toast: ToastrService,
    private spinner: NgxSpinnerService,
    private cookies: CookieService,
    private router: Router
  ) {}

  ngOnInit() {
    if (this.cookies.get("orianaAdminToken")) {
      this.router.navigate(["/dashboard"]);
    }
  }

  public openModal() {
    this.display = "block";
  }

  public onCloseHandled() {
    this.display = "none";
  }

  // Api to get forget password for Buyer

  public forgetPwdForm(formData) {
    if (formData) {
      let pattern: any = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      var data = formData["femail"];
      if (pattern.test(data)) {
        console.log(formData["femail"]);
        this.spinner.show();
        this.apis
          .post("/api/v1/admin/forgetpassword", { email: formData["femail"] })
          .subscribe(
            (res: Response) => {
              console.log("res");
              console.log(res);
              this.spinner.hide();
              this.display = "none";
              if (res["status"].toString() == "success") {
                this.cModal = 1;
                this.toast.success("Check Your Mail for Password");
              } else {
                this.toast.error("Some Problem Occured");
              }
            },
            (err: Response) => {
              this.display = "none";
              console.log("err");
              console.log(err);
              this.spinner.hide();
              this.toast.error("Something went wrong");
            }
          );
      } else {
        this.toast.error("Enter valid email");
      }
    }
  }
  /*  public cModalNull() {
    this.cModal = "";
  } */

  /*  public updatePasswordForm(formData) {
    if (formData) {
      //this.spinner.show()
      this.apis
        .post("/api/v1/admin/update-password", {
          email: formData["femail"],
          code: formData["code"],
          password: formData["password"]
        })
        .subscribe(
          (res: Response) => {
            console.log("res");
            console.log(res);
            //this.spinner.hide();
            this.cModal = "";
            this.onCloseHandled();
            if (res["status"].toString() == "success") {
              this.toast.success(res["message"]);
            } else {
              this.toast.error("Some Problem Occured");
            }
          },
          (err: Response) => {
            this.cModal = "";
            this.onCloseHandled();
            console.log("err");
            console.log(err);
            //this.spinner.hide();
            this.toast.error("Something went wrong");

            if (err["error"].message == "Errors") {
              this.toast.error(err["error"].data.errors[0].msg);
            } else {
              this.toast.error(err["error"].message);
            }
          }
        );
    }
  } */

  public doLogin(formData) {
    if (formData) {
      this.spinner.show();
      this.apis
        .post("/api/v1/admin/login", {
          email: formData["lemail"],
          password: formData["lpassword"]
        })
        .subscribe(
          (res: Response) => {
            this.spinner.hide();
            console.log(res);
            if (res["status"].toString() == "success") {
              //this.cookies.set("name", res["data"]["name"]);
              //this.cookies.set("userId", res["data"]["_id"]);
              //localStorage.setItem("orianaAdminToken", res["data"]["token"]);
              this.cookies.set("orianaAdminToken", res["data"]["token"]);
              this.apis.adminLogged = 1;
              //this.cookies.set("role", "admin");
              //this.apis.loggedIn = this.cookies.get("name");
              this.toast.success("Logged In");

              this.router.navigate(["/dashboard"]);
            } else {
              this.toast.error("Some Problem Occured");
            }
          },
          (res: Response) => {
            this.spinner.hide();
            if (res["error"].message == "Errors") {
              this.toast.error(res["error"].data.errors[0].msg);
            } else {
              this.toast.error(res["error"].message);
            }
          }
        );
    }
  }
}
